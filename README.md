
![](subwrite-logo.png)


# subwrite

A Ridiculously Personalized Markdown Editor by Sarah Jamie Lewis.

## Screenshots

![](subwrite.png)

![](subwrite-spellcheck.png)

## Features

- Reorderable Table of Contents / Section View
- Spellchecking / Custom Dictionary
- Inline Image Previews

## Contributing

Open an issue or submit a PR!
