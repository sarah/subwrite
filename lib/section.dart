class Section {
  int lineNumber;
  int level;
  String title;

  Section(this.lineNumber, this.level, this.title);
}
