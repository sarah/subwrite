import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:subwrite/theme.dart';

import 'file.dart';

class InputListener extends StatefulWidget {
  late Widget child;
  InputListener(this.child);
  @override
  _InputListener createState() => _InputListener();
}

class _InputListener extends State<InputListener> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void newTextDialog(context, String title, String hint, Function(String) callback) {
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        TextEditingController controller = TextEditingController();
        return AlertDialog(
          shape: ContinuousRectangleBorder(side: BorderSide(color: sidebarAlt)),
          backgroundColor: sidebar,
          title: Align(alignment: Alignment.topCenter, child: Text(title)),
          titleTextStyle: TextStyle(fontSize: 10.0),
          titlePadding: EdgeInsets.all(5.0),
          content: TextField(
            autofocus: true,
            controller: controller,
            onSubmitted: (newValue) {
              callback(newValue);
              Navigator.of(context).pop();
            },
            style: TextStyle(color: Colors.white, fontSize: 10.0),
            cursorColor: Colors.white,
            decoration: InputDecoration(
                hintText: hint,
                hintStyle: TextStyle(color: Colors.white38),
                isDense: true,
                fillColor: sidebarHighlight,
                focusColor: sidebarHighlight,
                labelStyle: TextStyle(color: Colors.white),
                enabledBorder: OutlineInputBorder(
                  borderSide: BorderSide(width: 1, color: sidebarAlt),
                  borderRadius: BorderRadius.zero,
                ),
                focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(width: 1, color: sidebarHighlight),
                  borderRadius: BorderRadius.zero,
                )),
          ),
          contentPadding: EdgeInsets.symmetric(vertical: 1.0, horizontal: 5.0),
          actions: [
            ElevatedButton.icon(
                icon: Icon(
                  Icons.label,
                  color: literal,
                ),
                onPressed: () {
                  controller.text = "[edit]${controller.text}";
                  callback(controller.text);
                  Navigator.of(context).pop();
                },
                label: Text("Edit"),
                style: ButtonStyle(backgroundColor: MaterialStateProperty.all(tabs), foregroundColor: MaterialStateProperty.all(foreground))),
            ElevatedButton.icon(
                icon: Icon(
                  Icons.label,
                  color: constant,
                ),
                onPressed: () {
                  controller.text = "[incomplete]${controller.text}";
                  callback(controller.text);
                  Navigator.of(context).pop();
                },
                label: Text("Incomplete"),
                style: ButtonStyle(backgroundColor: MaterialStateProperty.all(tabs), foregroundColor: MaterialStateProperty.all(foreground))),
          ],
        );
      },
    ).then((value) => null);
  }

  @override
  Widget build(BuildContext context) {
    LineFile doc = Provider.of<LineFile>(context);

    return GestureDetector(
        child: Focus(
            focusNode: doc.focus,
            autofocus: true,
            onKey: (FocusNode node, RawKeyEvent event) {
              doc.handleKey(event);
              if (event.logicalKey == LogicalKeyboardKey.escape) {
                newTextDialog(context, "New Note", "Note", (note) => {doc.addNote(note)});
              }
              return KeyEventResult.handled;
            },
            child: widget.child));
  }
}
