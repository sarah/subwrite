import 'dart:io';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:subwrite/file.dart';
import 'package:subwrite/theme.dart';
import 'package:subwrite/view.dart';

class Outline extends StatefulWidget {
  const Outline({super.key});

  @override
  State<Outline> createState() => _Outline();
}

class _Outline extends State<Outline> {
  final ScrollController controller = ScrollController();

  @override
  Widget build(BuildContext context) {
    var sections = Provider.of<LineFile>(context).sections;

    return Material(
        color: tabs,
        child: LayoutBuilder(builder: (context, constraints) {
          return Scrollbar(
              controller: controller,
              thumbVisibility: false,
              trackVisibility: false,
              child: ReorderableListView.builder(
                scrollController: controller,
                physics: AlwaysScrollableScrollPhysics(),
                header: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
                    child: Column(children: [
                      Row(mainAxisSize: MainAxisSize.min, mainAxisAlignment: MainAxisAlignment.center, children: [
                        // Icon(
                        //   Icons.edit,
                        //   color: foreground,
                        // ),
                        Image(
                          image: AssetImage("assets/subwrite-logo.png"),
                          filterQuality: FilterQuality.medium,
                          width: constraints.maxWidth / 3.0,
                          height: constraints.maxWidth / 3.0,
                        )
                        //Text("subwrite", style: TextStyle(fontSize: 16.0, color: foreground, fontWeight: FontWeight.bold, fontFamily: "Iosevka"))
                      ]),
                      SizedBox(
                        height: 20,
                      ),
                      Row(mainAxisSize: MainAxisSize.min, mainAxisAlignment: MainAxisAlignment.center, children: [
                        TextButton(
                            onPressed: () {
                              LineFile doc = Provider.of<LineFile>(context, listen: false);
                              newFile(doc);
                            },
                            child: Text("New", style: TextStyle(fontSize: 10.0, color: foreground, fontWeight: FontWeight.bold, fontFamily: "Iosevka"))),
                        TextButton(
                            onPressed: () {
                              LineFile doc = Provider.of<LineFile>(context, listen: false);
                              openFile(doc);
                            },
                            child: Text("Open", style: TextStyle(fontSize: 10.0, color: foreground, fontWeight: FontWeight.bold, fontFamily: "Iosevka"))),
                        TextButton(
                            onPressed: () {
                              exit(0);
                            },
                            child: Text("Exit", style: TextStyle(fontSize: 10.0, color: foreground, fontWeight: FontWeight.bold, fontFamily: "Iosevka"))),
                      ]),
                      Divider(
                        color: foreground,
                      )
                    ])),
                footer: Padding(padding: const EdgeInsets.symmetric(vertical: 3.0, horizontal: 10.0), child: Divider(color: foreground, thickness: 4.0)),
                onReorder: (int oldIndex, int newIndex) {
                  var doc = Provider.of<LineFile>(context, listen: false);
                  var startLine = sections[oldIndex].lineNumber;

                  var endLine = doc.lines();
                  for (var index = oldIndex + 1; index < sections.length; index++) {
                    if (sections[oldIndex].level < sections[index].level) {
                      // sub section...skipping
                    } else {
                      endLine = sections[index].lineNumber;
                      break;
                    }
                  }

                  var newLine = newIndex >= sections.length ? doc.lines() : sections[newIndex].lineNumber;

                  doc.moveLines(startLine, endLine, newLine);
                },
                buildDefaultDragHandles: false,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    key: ValueKey(index),
                    tileColor: tabs,
                    title: Text(
                      sections[index].title.padLeft(sections[index].title.length + sections[index].level - 1, '  '),
                      style: TextStyle(color: foreground, fontSize: 14.0 - sections[index].level, fontFamily: "Iosevka"),
                    ),
                    onTap: () {
                      Provider.of<LineFile>(context, listen: false).scrollController.jumpTo(index: sections[index].lineNumber - 1);
                    },
                    trailing: ReorderableDragStartListener(
                      index: index,
                      child: Icon(
                        Icons.drag_handle,
                        color: foreground,
                      ),
                    ),
                  );
                },
                itemCount: sections.length,
              ));
        }));
  }
}
