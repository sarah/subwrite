import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:subwrite/file.dart';
import 'package:subwrite/theme.dart';

import 'editor.dart';
import 'outline.dart';

void main(List<String> args) {
  var docfile = LineFile();
  docfile.buildDictionary();
  if (args.length > 0) {
    docfile.openFile(args[0], 0);
  }
  var doc = ChangeNotifierProvider.value(value: docfile);

  runApp(MultiProvider(
      providers: [doc],
      builder: (context, child) {
        return MyApp();
      }));
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'subwrite',
      theme: ThemeData(
          scrollbarTheme: ScrollbarThemeData(
        thumbColor: MaterialStateProperty.all(header),
        thumbVisibility: MaterialStateProperty.all(true),
      )),
      home: const SarahDownApp(),
    );
  }
}

class SarahDownApp extends StatefulWidget {
  const SarahDownApp({super.key});

  @override
  State<SarahDownApp> createState() => _SarahDownApp();
}

class _SarahDownApp extends State<SarahDownApp> {
  @override
  Widget build(BuildContext context) {
    return Material(
        color: sidebarAlt,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Expanded(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Flexible(
                  flex: 2,
                  child: Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [Expanded(child: Outline()), Container(color: comment)])),
              Flexible(flex: 5, child: Editor())
            ],
          )),
          Container(
              height: 16,
              decoration: BoxDecoration(color: sidebarAlt, border: Border(top: BorderSide(width: 1, color: border))),
              child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 2.0, horizontal: 5.0),
                  child: Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                    Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
                      SizedBox(
                          width: 200,
                          child: LinearProgressIndicator(
                            color: comment,
                            backgroundColor: sidebar,
                            value: Provider.of<LineFile>(context).statusProgress,
                            minHeight: 5.0,
                          )),
                      SizedBox(
                        width: 10,
                      ),
                      Text(
                        "${Provider.of<LineFile>(context).status}",
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          fontFamily: 'Iosevka',
                          fontSize: 8,
                          fontWeight: FontWeight.bold,
                          color: foreground,
                          backgroundColor: Colors.transparent,
                        ),
                      ),
                    ]),
                    Text(
                      "Word Count: ${Provider.of<LineFile>(context).wordCount()}",
                      textAlign: TextAlign.right,
                      style: TextStyle(
                        fontFamily: 'Iosevka',
                        fontSize: 10,
                        color: foreground,
                        backgroundColor: Colors.transparent,
                      ),
                    )
                  ])))
        ]));
  }
}
