import 'dart:ui';

Color foreground = const Color(0xfff8f8f2);
Color background = const Color(0xff111111); //Color(0xFF1A0F1C);
Color comment = const Color(0xffd684e8);
Color selection = const Color(0xff777889);

Color constant = const Color(0xff8be9fd);
Color header = const Color(0xffbd93f9);
Color keyword = const Color(0xffffb86c);
Color variable = const Color(0xffff79c6);
Color function = const Color(0xfff1fa8c);
Color literal = const Color(0xff50fa7b);
Color field = header;

Color risk = const Color(0xff45353b);

Color sidebarAlt = const Color(0xFF070607);
Color sidebar = const Color(0xff202023);
Color border = const Color(0xff202023);
Color tabs = const Color(0xFF342036);
Color sidebarHighlight = const Color(0xFF38213D);
