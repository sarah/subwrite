class SourceLink {
  late final String file;
  final int? lineStart;
  final int? colStart;
  final int? lineEnd;
  final int? colEnd;

  SourceLink(this.file, {this.lineStart, this.colStart, this.lineEnd, this.colEnd});

  String lineLink() {
    return file + ":" + lineStart.toString();
  }

  @override
  bool operator ==(Object other) {
    return other is SourceLink && other.hashCode == hashCode;
  }

  @override
  int get hashCode {
    var hash = Object.hash(file, lineStart, colStart, lineEnd, colEnd);
    return hash;
  }
}

SourceLink? parseSourceLink(String line) {
  String? file;
  int? lineStart;
  int? colStart;
  int? lineEnd;
  int? colEnd;
  var parts = line.split(":");
  if (parts.isNotEmpty && parts[0].isNotEmpty) {
    file = parts[0];
  } else {
    return null;
  }
  if (parts.length > 1 && parts[1].trim().isNotEmpty) {
    lineStart = int.parse(parts[1]);
  }
  if (parts.length > 2 && parts[2].trim().isNotEmpty) {
    colStart = int.parse(parts[2]);
  }
  if (parts.length > 3 && parts[3].trim().isNotEmpty) {
    lineEnd = int.parse(parts[3]);
  }
  if (parts.length > 4 && parts[4].trim().isNotEmpty) {
    colEnd = int.parse(parts[4]);
  }
  return SourceLink(file, lineStart: lineStart, lineEnd: lineEnd, colStart: colStart, colEnd: colEnd);
}
