import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:subwrite/theme.dart';
import 'package:subwrite/view.dart';
import 'file.dart';

class Ghostsense extends StatefulWidget {
  List<Suggestion> suggestions;
  Ghostsense(this.suggestions);
  @override
  _Ghostsense createState() => _Ghostsense();
}

class _Ghostsense extends State<Ghostsense> {
  @override
  Widget build(BuildContext context) {
    return Material(
        type: MaterialType.transparency,
        child: ListView.builder(
            controller: Provider.of<LineFile>(context).suggestionListController,
            physics: const BouncingScrollPhysics(),
            itemCount: widget.suggestions.length,
            padding: EdgeInsets.all(2.0),
            itemBuilder: (BuildContext bcontext, int index) {
              return Listener(
                  onPointerDown: (event) {
                    var suggestion = widget.suggestions[index];
                    suggestion.callback(bcontext, suggestion);
                    Provider.of<LineFile>(bcontext).cancelSuggestions();
                  },
                  onPointerHover: (event) {
                    Provider.of<LineFile>(bcontext).setSuggestIndex(index);
                  },
                  child: Container(color: Provider.of<LineFile>(context).suggestionIndex == index ? sidebarHighlight : sidebar, child: widget.suggestions[index].getWidget()));
            }));
  }
}
