import 'package:flutter/material.dart';
import 'package:subwrite/sourcelink.dart';
import 'package:subwrite/theme.dart';

enum AnnotationType {
  highlight,
  error,
  todo,
  note,
  image,
}

class LineAnnotation {
  SourceLink sourceLink;
  AnnotationType annotationType;
  String type;
  SourceLink? reflink;
  String? description;

  // the tool that generated this annotation
  String tool;

  @override
  bool operator ==(Object other) {
    return other is LineAnnotation && other.annotationType == annotationType && other.description == description && other.sourceLink == sourceLink;
  }

  @override
  int get hashCode {
    var hash = Object.hash(sourceLink, annotationType, description);
    return hash;
  }

  LineAnnotation(this.sourceLink, this.tool, this.annotationType, this.type, {this.reflink, this.description});

  Icon getIcon(double size) {
    if (type == "note") {
      return Icon(
        Icons.note,
        color: literal,
        size: size,
      );
    }
    if (type == "todo") {
      return Icon(
        Icons.details,
        color: variable,
        size: size,
      );
    }
    if (type == "incomplete") {
      return Icon(
        Icons.label,
        color: constant,
        size: size,
      );
    }
    if (type == "edit") {
      return Icon(
        Icons.label,
        color: literal,
        size: size,
      );
    }
    return Icon(
      Icons.warning_amber_outlined,
      color: Colors.red,
      size: size,
    );
  }
}
