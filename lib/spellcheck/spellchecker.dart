import 'dart:collection';
import 'dart:io';
import 'dart:isolate';

import 'package:flutter/foundation.dart';

class DictionaryLoadRequest {
  String dictPath;
  SendPort callback;
  DictionaryLoadRequest(this.dictPath, this.callback);
}

class SpellChecker {
  HashMap<String, HashSet<String>> dictionary;
  HashSet<String> known;
  HashSet<String> custom;

  SpellChecker(this.dictionary, this.known, this.custom);

  static SpellChecker load(DictionaryLoadRequest request) {
    HashMap<String, HashSet<String>> dictionary = HashMap();
    HashSet<String> known = HashSet();
    HashSet<String> custom = HashSet();
    File dict = File(request.dictPath);

    var lines = dict.readAsLinesSync();

    print("building dictionary...");
    for (int i = 0; i < lines.length; i++) {
      var word = lines[i];
      var lower = word.toLowerCase().trim();
      dictionary.putIfAbsent(lower, () => HashSet.from([lower]));
      known.add(lower);

      mutations1(lower).forEach((element) {
        dictionary.putIfAbsent(element, () => HashSet());
        dictionary[element]!.add(lower);
      });

      if (i % 1000 == 0) {
        request.callback.send(i.toDouble() / lines.length);
      }
    }

    File customdict = File("subwrite.custom.dictionary.txt");
    print("building custom dictionary...");
    if (customdict.existsSync()) {
      customdict.readAsLinesSync().forEach((word) {
        var lower = word.toLowerCase().trim();
        dictionary.putIfAbsent(lower, () => HashSet.from([lower]));
        known.add(lower);
        custom.add(lower);

        mutations1(lower).forEach((element) {
          dictionary.putIfAbsent(element, () => HashSet());
          dictionary[element]!.add(lower);
        });
      });
    }

    // finished!
    request.callback.send(1.0);
    print("built dictionary...${dictionary.length}");
    return SpellChecker(dictionary, known, custom);
  }

  HashSet<String>? candidates(String word) {
    return dictionary[word];
  }

  void addToCustomDictionary(String name) {
    print("adding $name to custom dictionary");
    var lower = name.toLowerCase().trim();
    dictionary.putIfAbsent(lower, () => HashSet.from([lower]));
    known.add(lower);

    mutations1(lower).forEach((element) {
      dictionary.putIfAbsent(element, () => HashSet());
      dictionary[element]!.add(lower);
    });

    custom.add(name);

    File("subwrite.custom.dictionary.txt").writeAsStringSync(custom.join("\n"));
  }
}

HashSet<String> mutations1(String word) {
  // "All edits that are one edit away from `word`."
  var letters = 'abcdefghijklmnopqrstuvwxyz';

  HashSet<String> mutations = HashSet();

  // // delete a letter
  for (int i = 0; i < word.length; i++) {
    mutations.add(word.substring(0, i) + word.substring(i + 1));
  }

  // transpositions
  for (int i = 0; i < word.length - 1; i++) {
    mutations.add(word.substring(0, i) + word.substring(i + 1, i + 2) + word.substring(i, i + 1) + word.substring(i + 2));
  }

  // insert a letter
  for (int i = 0; i < word.length; i++) {
    for (var c = 0; c < 25; c++) {
      mutations.add(word.substring(0, i) + letters.substring(c, c + 1) + word.substring(i));
    }
  }

  // replace a letter
  for (int i = 0; i < word.length; i++) {
    for (var c = 0; c < 25; c++) {
      mutations.add(word.substring(0, i) + letters.substring(c, c + 1) + word.substring(i + 1));
    }
  }

  return mutations;
}

HashSet<String> mutations2(String word) {
  HashSet<String> mutations = HashSet();
  for (var e2 in mutations1(word)) {
    mutations.addAll(mutations1(e2));
  }
  return mutations;
}
